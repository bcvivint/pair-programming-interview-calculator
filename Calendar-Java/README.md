# Calendar View Pair Programming Project

## Bugs Introduced

1. Calendar with Decorators - There is a red dot on the dates with events. The dots are too small, increase the size to something more visible. This is really an exercise in digging in the code as the fix is easy it’s just buried a few levels deep.

2. Basic Example - “No selection” is shown at the bottom of the basic activity even when a date is selected. Dates should be shown and properly formatted (Wed, 23 Jan 2019 or whatever you want to do)

3. Calendar with Dynamic Modes - Selected dates have larger text, when new dates are selected the text size is not properly changed. I pulled out where we set the data changed listener

4. Calendar Selection Modes - Add titles to the different calendars to indicate the selection type

5. Rotation Example - Crashes when the activity is run. The activity tries to bind to the R.id.calendarView item, but it doesn’t exist

### Check out commit #8498be56bb9 to see the changes made to introduce these bugs.